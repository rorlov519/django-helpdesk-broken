import logging
import psycopg2
import uuid
import time
from psycopg2.extensions import cursor, connection
from faker import Faker

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] %(levelname)-12s|process:%(process)-5s|thread:%(thread)-5s|funcName:%(funcName)s|message:%(message)s",
    handlers=[
        # logging.FileHandler('fileName.log'),
        logging.StreamHandler()
    ])


def log_wrapper(wrapped_func):
    def wrapper(*args):
        logging.info(f'STARTING {wrapped_func.__name__} ...')
        start_func_time = round(time() * 1000)
        wrapped_func(*args)
        end_func_time = round(time() * 1000)
        work_time_milli = end_func_time - start_func_time
        logging.info(
            f'ENDED {wrapped_func.__name__} in {work_time_milli:_} millis')
    return wrapper


class HelpDeskUser():
    def __init__(self, is_admin: bool):
        faker = Faker()
        user_random_name = faker.name()
        while len(user_random_name.split(' ')) != 2:
            user_random_name = faker.name()
        self.__user_name, self.__user_surname = user_random_name.split(' ')
        self.__email = faker.ascii_email()
        self.__is_admin = is_admin
        logging.info(
            f'Created HelpDeskUser: user_name={self.__user_name}, user_surname={self.__user_surname} email={self.__email} is_admin={self.__is_admin}')


def generate_users(user_counts: int, is_admin: bool, ):
    pass


def main():
    # faker = Faker(locale='ru_RU')
    Faker.seed(4321)
    for x in range(10):
        user = HelpDeskUser(True)
    # name = faker.name()

    # logging.info(f'name <type:{type(name)}> = {name}')
    # text = faker.text()
    # logging.info(f'text <type:{type(text)}> = {text}')

    # name2 = faker.name()
    # logging.info(f'name2 <type:{type(name2)}> = {name2}')

    # conn: connection = psycopg2.connect(database="postgres", user="postgres",
    #                                     password="postgresdev", host="workbench.lanit.ru", port=23233)
    # logging.info(f'conn <type:{type(conn)}> = {conn}')
    # try:
    #     with conn.cursor() as curr:
    #         curr: cursor

    #         curr.execute("SELECT version()")
    #         data = curr.fetchall()
    #         logging.info(f'data = {data}')

    #         curr.execute("SELECT MAX(id) from helpdesk_ticket")
    #         latest_ticket_number = int(curr.fetchall()[0][0])
    #         logging.info(f'latest_ticket_number = {latest_ticket_number}')

    #         for x in range(100):
    #             if x % 10 == 0:
    #                 logging.info(f'x = {x}')
    #             curr.execute(f"INSERT INTO public.helpdesk_ticket (title, created, modified, submitter_email, status, on_hold, description, resolution, priority, due_date, last_escalation, assigned_to_id, queue_id, secret_key, kbitem_id, merged_to_id) VALUES('Something with an attachment', '2017-03-20 08:14:36.320', '2017-03-20 08:28:28.695', 'helpdesk@example.com', 1, false, 'WHOA!', NULL, 1, NULL, NULL, NULL, 1, '{str(uuid.uuid4())}', NULL, NULL);")

    #         logging.info(f'curr <type:{type(curr)}> = {curr}')
    #         curr.close()
    #         conn.commit()
    # finally:
    #     conn.close()


if __name__ == "__main__":
    main()
